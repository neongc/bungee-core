package ru.upworld.log;

import java.util.logging.Level;
import net.md_5.bungee.BungeeCord;

public class LoggerManager {
    
    public void log(String name, String msg) {
        BungeeCord.getInstance().getLogger().log(Level.INFO, "§9[§3Up-world CORE§9] [§3{0}§9] §4{1}", new Object[]{name, msg});
    }

    public void log(String msg) {
        BungeeCord.getInstance().getLogger().log(Level.INFO, "§9[§3Up-world CORE§9] §4{0}", msg);
    }
}
