package ru.upworld.log;

import java.util.logging.Level;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;

public class NetworkLogger {
    
    public static void info(String msg) {
        BungeeCord.getInstance().getLogger().log(Level.INFO, "§9[§3Up-world CORE§9] §9[§6Netty Client§9] {0}{1}", new Object[]{ChatColor.DARK_GREEN, msg});
    }

    public static void warning(String msg) {
        BungeeCord.getInstance().getLogger().log(Level.WARNING, "§9[§3Up-world CORE§9] §9[§6Netty Client§9] {0}{1}", new Object[]{ChatColor.DARK_RED, msg});
    }
    
    public static void warning(Throwable th, Class<?> clazz) {
        StackTraceElement[] trace = th.getStackTrace();
        for (StackTraceElement traceElement : trace) BungeeCord.getInstance().getLogger().log(Level.WARNING, "§9[§3Up-world CORE§9] §9[§6Netty Client§9] [§5{0}§9] {1}\tat {2}", new Object[]{clazz.getSimpleName(), ChatColor.DARK_RED, traceElement});
    }
}
