package ru.upworld.network.filter;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import ru.upworld.network.PacketBuffer;
import ru.upworld.network.packets.Packet;

public class PacketEncoder extends MessageToByteEncoder<Packet<?>> {

    @Override
    protected void encode(ChannelHandlerContext ctx, Packet<?> packet, ByteBuf buf) throws Exception {
        PacketBuffer pbuf = new PacketBuffer(buf);
        packet.writePacketData(pbuf);
    }
}
