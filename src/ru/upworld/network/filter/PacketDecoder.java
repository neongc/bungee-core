package ru.upworld.network.filter;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import java.util.List;
import ru.upworld.network.PacketBuffer;
import ru.upworld.network.packets.Packet;
import ru.upworld.network.packets.State;

public class PacketDecoder extends ByteToMessageDecoder {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf buf, List<Object> out) throws Exception {
        if (buf.readableBytes() != 0) {
            PacketBuffer pbuf = new PacketBuffer(buf);
            int id = pbuf.readVarInt();
            Packet<?> packet = State.getPacket(id);
            packet.readPacketData(pbuf);
            out.add(packet);
        }
    }
}
