package ru.upworld.network.filter;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import ru.upworld.network.handlers.ClientHandler;
import ru.upworld.network.filter.PacketDecoder;
import ru.upworld.network.filter.PacketEncoder;

public class ClientPipeline extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        pipeline.addLast("decoder", new PacketDecoder());
        pipeline.addLast("encoder", new PacketEncoder());
        pipeline.addLast("handler", new ClientHandler());
    }
}
