package ru.upworld.network.handlers;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.io.IOException;
import ru.upworld.UpWorldBungee;
import ru.upworld.log.NetworkLogger;
import ru.upworld.network.ContainerClient;
import ru.upworld.network.ThreadQuickExitException;
import ru.upworld.network.packets.Packet;
import ru.upworld.network.packets.ServerConnectPacket;

public class ClientHandler extends SimpleChannelInboundHandler<Packet<?>> {

    private static Channel channel;
    public static boolean connected;

    public void closeChannel() {
        if (channel.isOpen()) {
            channel.close().awaitUninterruptibly();
            connected = false;
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext channel) throws Exception {
        super.channelActive(channel);
        ClientHandler.channel = channel.channel();
        NetworkLogger.info("-----------------------------------");
        NetworkLogger.info("      Коннект был установлен!      ");
        NetworkLogger.info("Отправляю данные о сервере в кор...");
        NetworkLogger.info("-----------------------------------");
        sendPacket(new ServerConnectPacket(UpWorldBungee.server.getName()));
        NetworkLogger.info("-----------------------------------");
        NetworkLogger.info("  Данные были успешно отправлены!  ");
        NetworkLogger.info("-----------------------------------");
        connected = true;
    }

    @Override
    public void channelInactive(ChannelHandlerContext channel) throws Exception {
        closeChannel();
        NetworkLogger.warning("-----------------------------------");
        NetworkLogger.warning("       Коннект был разорван!       ");
        if (UpWorldBungee.getInstance().client.isRunning(ContainerClient.Action.CUSTOMSHUTDOWN)) {
            NetworkLogger.warning("   Запускаю повторный коннект...   ");
            UpWorldBungee.getInstance().client.start();
        }
        NetworkLogger.warning("-----------------------------------");
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext channel, Throwable cause) throws Exception {
        closeChannel();
        NetworkLogger.warning("-------------------------------");
        NetworkLogger.warning("Коннект был разорван с ошибкой!");
        NetworkLogger.warning(cause.getMessage());
        NetworkLogger.warning("-------------------------------");
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void channelRead0(ChannelHandlerContext channel, Packet<?> packet) throws IOException {
        if (ClientHandler.channel.isOpen()) {
            try {
                packet.processPacket(channel.channel());
            } catch (ThreadQuickExitException ex) {
                ex.printStackTrace();
            }
        }
    }

    public static void sendPacket(Channel channel, Packet<?> packet) {
        channel.writeAndFlush(packet);
    }

    public static void sendPacket(Packet<?> packet) {
        ClientHandler.channel.writeAndFlush(packet);
    }
}
