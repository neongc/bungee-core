package ru.upworld.network.packets;

import io.netty.channel.Channel;
import java.io.IOException;
import net.md_5.bungee.BungeeCord;
import ru.upworld.network.PacketBuffer;

public class ShutdownPacket implements Packet {

    @Override
    public void readPacketData(PacketBuffer buf) throws IOException {
    }

    @Override
    public void writePacketData(PacketBuffer buf) throws IOException {
        buf.writeVarInt(State.getPacketId(this));
    }

    @Override
    public void processPacket(Channel channel) {
        BungeeCord.getInstance().stop();
    }
    
}
