package ru.upworld.network.packets;

import io.netty.channel.Channel;
import java.io.IOException;
import ru.upworld.network.PacketBuffer;

public class ServerConnectPacket implements Packet {
    
    public String server;

    public ServerConnectPacket(String server) {
        this.server = server;
    }

    @Override
    public void readPacketData(PacketBuffer buf) throws IOException {
    }

    @Override
    public void writePacketData(PacketBuffer buf) throws IOException {
        buf.writeVarInt(State.getPacketId(this));
        buf.writeString(this.server);
    }

    @Override
    public void processPacket(Channel channel) {
    }
    
}
