package ru.upworld.network.packets;

import io.netty.channel.Channel;
import java.io.IOException;
import net.md_5.bungee.BungeeCord;
import ru.upworld.UpWorldBungee;
import ru.upworld.network.PacketBuffer;
import ru.upworld.network.handlers.ClientHandler;

public class OnlinePacket implements Packet {

    public String server;
    public int online;

    @Override
    public void readPacketData(PacketBuffer buf) throws IOException {
    }

    @Override
    public void writePacketData(PacketBuffer buf) throws IOException {
        buf.writeVarInt(State.getPacketId(this));
        buf.writeString(this.server);
        buf.writeInt(this.online);
    }

    @Override
    public void processPacket(Channel channel) {
        this.server = UpWorldBungee.server.getName();
        this.online = BungeeCord.getInstance().getOnlineCount();
        ClientHandler.sendPacket(this);
    }
}
