package ru.upworld.network.packets;

import java.util.HashMap;
import java.util.Map;
import ru.upworld.log.NetworkLogger;
import ru.upworld.network.packets.player.PlayerLoginPacket;
import ru.upworld.network.packets.player.RedirectPacket;

public class State {

    private static final Map<Integer, Class<? extends Packet>> packets = new HashMap();
    private static boolean get = false;

    public static int getPacketId(Packet<?> packet) {
        Map<Class<? extends Packet>, Integer> inverse = new HashMap();
        packets.entrySet().forEach((id) -> {
            inverse.put(id.getValue(), id.getKey());
            if (id.getValue() == packet.getClass()) {
                get = true;
            }
        });
        if (!get || inverse.get(packet.getClass()) == 0 || inverse.get(packet.getClass()) == null) {
            NetworkLogger.warning("Пакета " + packet.getClass() + " нет в списке!");
            NetworkLogger.info("Загружаю пакет...");
            addPacket(packet.getClass());
            return getPacketId(packet);
        }
        return inverse.get(packet.getClass());
    }

    public static Packet<?> getPacket(int i) throws InstantiationException, IllegalAccessException {
        Class<? extends Packet> oclass = packets.get(i);
        return oclass == null ? null : (Packet<?>) oclass.newInstance();
    }

    public static void addPacket(Class pack) {
        Integer id = namePacketToNum(pack.getSimpleName());
        if (!packets.containsKey(id)) {
            packets.put(id, pack);
        } else {
            try {
                throw new Exception("New packet now exists! Check name your module! Packet name: " + pack.toString());
            } catch (Exception ex) {
                NetworkLogger.warning("-------------------------------");
                NetworkLogger.warning(ex, State.class);
                NetworkLogger.warning("-------------------------------");
            }
        }
    }

    private static Integer namePacketToNum(String name) {
        byte[] b = name.getBytes();
        Integer deced = 0;
        for (int i = 0; i < b.length; i++) {
            deced = deced + (b[i] & 0xff) * i;
        }
        return deced;
    }

    static {
        addPacket(OnlinePacket.class);
        addPacket(PlayerLoginPacket.class);
        addPacket(RedirectPacket.class);
        addPacket(ServerConnectPacket.class);
    }
}