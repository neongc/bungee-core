package ru.upworld.network.packets.player;

import io.netty.channel.Channel;
import java.io.IOException;
import ru.upworld.network.PacketBuffer;
import ru.upworld.network.packets.Packet;
import ru.upworld.network.packets.State;

public class PlayerLoginPacket implements Packet {

    public String name;
    public String ip;

    public PlayerLoginPacket(String name, String ip) {
        this.name = name;
        this.ip = ip;
    }

    @Override
    public void readPacketData(PacketBuffer buf) throws IOException {
    }

    @Override
    public void writePacketData(PacketBuffer buf) throws IOException {
        buf.writeVarInt(State.getPacketId(this));
        buf.writeString(name);
        buf.writeString(ip);
    }

    @Override
    public void processPacket(Channel channel) {
    }
}
