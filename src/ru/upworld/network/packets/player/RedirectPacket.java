package ru.upworld.network.packets.player;

import io.netty.channel.Channel;
import java.io.IOException;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import ru.upworld.network.PacketBuffer;
import ru.upworld.network.packets.Packet;

public class RedirectPacket implements Packet {

    public String server;
    public String player;

    @Override
    public void readPacketData(PacketBuffer buf) throws IOException {
        this.server = buf.readString(256);
        this.player = buf.readString(256);
    }

    @Override
    public void writePacketData(PacketBuffer buf) throws IOException {
    }

    @Override
    public void processPacket(Channel channel) {
        ProxiedPlayer p = BungeeCord.getInstance().getPlayer(player);
        BungeeCord.getInstance().getServers().forEach((key, value) -> {
            if (!server.equalsIgnoreCase(key)) return;
            p.connect(value);
        });
    }
}
