package ru.upworld.network.packets;

import io.netty.channel.Channel;
import java.io.IOException;
import ru.upworld.network.PacketBuffer;

public interface Packet<T> {

    public void readPacketData(PacketBuffer buf) throws IOException;

    public void writePacketData(PacketBuffer buf) throws IOException;

    public void processPacket(Channel channel);
}
