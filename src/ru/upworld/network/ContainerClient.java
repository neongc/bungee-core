package ru.upworld.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import java.util.concurrent.TimeUnit;
import ru.upworld.log.NetworkLogger;
import ru.upworld.network.filter.ClientPipeline;
import ru.upworld.network.handlers.ClientHandler;

public class ContainerClient {

    private ChannelFuture channelFuture;
    private final Bootstrap bootstrap;
    private final Class<? extends SocketChannel> socketChannel;
    private final String server;
    private final int port;
    private final EventLoopGroup producer;
    private boolean customShutdown;

    public ContainerClient(String server, int port) {
        this.server = server;
        this.port = port;
        boolean hasEpoll = Epoll.isAvailable();
        if (hasEpoll) {
            producer = new EpollEventLoopGroup();
            socketChannel = EpollSocketChannel.class;
        } else {
            producer = new NioEventLoopGroup();
            socketChannel = NioSocketChannel.class;
        }
        bootstrap = new Bootstrap()
                .group(producer)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .option(ChannelOption.TCP_NODELAY, true)
                .channel(socketChannel)
                .handler(new ClientPipeline());
        NetworkLogger.info("Сокет: " + socketChannel.getSimpleName());
    }

    public void stop() {
        if (isRunning(Action.SHUTDOWN)) {
            customShutdown = true;
            NetworkLogger.warning("Остановка клиента");
            producer.shutdownGracefully();
        }
    }

    public void start() {
        channelFuture = bootstrap.connect(server, port);
        channelFuture.addListener((future) -> {
            if (future.isSuccess()) {
                channelFuture.channel().closeFuture();
                customShutdown = false;
                return;
            }
            NetworkLogger.warning("-------------------------------");
            NetworkLogger.warning("Коннект не был произведён. Ошибка: " + future.cause().getMessage());
            NetworkLogger.warning("Повторный коннект через 5 секунд");
            NetworkLogger.warning("-------------------------------");
            TimeUnit.SECONDS.sleep(5);
            start();
        });
    }

    public boolean isRunning(Action action) {
        switch (action) {
            case SHUTDOWN:
                if (producer != null) {
                    return !producer.isShutdown();
                } else {
                    return false;
                }
            case CUSTOMSHUTDOWN:
                return !customShutdown;
            case CONNECTED:
                return ClientHandler.connected;
            default:
                return false;
        }
    }

    public ChannelFuture getFuture() {
        return channelFuture;
    }
    
    public enum Action {
        SHUTDOWN, CUSTOMSHUTDOWN, CONNECTED;
        
        private Action(){
        }
    }
}
