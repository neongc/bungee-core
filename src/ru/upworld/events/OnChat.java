package ru.upworld.events;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.event.EventHandler;

public class OnChat {
    @EventHandler
    public void onChat(ChatEvent event) {
        if (((event.getMessage().equals("/end")) || (event.getMessage().startsWith("/end "))) && (event.getSender() instanceof ProxiedPlayer)) {
            ((ProxiedPlayer) event.getSender()).sendMessage(TextComponent.fromLegacyText(ChatColor.RED + "Нарушение безопасности!"));
            event.setCancelled(true);
        }
    }
}
