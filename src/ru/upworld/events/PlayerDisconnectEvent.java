package ru.upworld.events;

import net.md_5.bungee.api.plugin.Event;
import ru.upworld.data.Player;

public class PlayerDisconnectEvent extends Event {

    private final Player player;

    public PlayerDisconnectEvent(Player gamer) {
        this.player = gamer;
    }

    public Player getPlayer() {
        return this.player;
    }

}
