package ru.upworld.events;

import net.md_5.bungee.api.plugin.Event;
import ru.upworld.api.enums.LoadType;
import ru.upworld.data.Player;

public class PlayerLoadEvent extends Event {

    private final Player player;
    private final LoadType loadType;

    public PlayerLoadEvent(Player player, LoadType loadType) {
        this.player = player;
        this.loadType = loadType;
    }

    public Player getPlayer() {
        return this.player;
    }

    public LoadType getLoadType() {
        return this.loadType;
    }
}
