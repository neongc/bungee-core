package ru.upworld.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.api.modules.UpWorldCommand;
import ru.upworld.utils.ChatUtil;

public class UpRestartCommand extends UpWorldCommand {
    
    private static boolean command;
    
    public UpRestartCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "uprestart", "upworld.restart", true);
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("all")) {
                sender.sendMessage(ChatColor.DARK_AQUA + "Вы уверены? Все сервера пойдут на перезагрузку!");
                sender.sendMessage(ChatUtil.printRunCommand(ChatColor.RED + "Да", "/uprestartasdasdasdyes"), new TextComponent(" "), ChatUtil.printRunCommand(ChatColor.GREEN + "Нет", "/uprestartasdasdasdno"));
                command = true;
            } else {
                sender.sendMessage(ChatColor.DARK_AQUA + "Вы уверены? Сервер \"" + ChatColor.YELLOW + args[0] + "\"" + ChatColor.DARK_AQUA + " будет перезагружен!");
                sender.sendMessage(ChatUtil.printRunCommand(ChatColor.RED + "Да", "/uprestartasdasdasdyes " + args[0]), new TextComponent(" "), ChatUtil.printRunCommand(ChatColor.GREEN + "Нет", "/uprestartasdasdasdno"));
                command = true;
            }
            return;
        }
        sender.sendMessage(ChatColor.RED + "Использование: /restart <server>|all");
    }

    public static class UpAnswerYes extends UpWorldCommand {
        
        public UpAnswerYes(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "uprestartasdasdasdyes", "upworld.restart");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            if (args.length == 1) {
                sender.sendMessage(ChatColor.DARK_RED + "Вы запустили перезагрузку сервера \"" + ChatColor.YELLOW + args[0] + "\"!");
                getCore().getMessagesHandler().sendRestart(args[0]);
            } else {
                sender.sendMessage(ChatColor.DARK_RED + "Вы запустили перезагрузку всех серверов!");
                getCore().getMessagesHandler().sendRestart(args[0]);
            }
        }
    }

    public static class UpAnswerNo extends UpWorldCommand {
        
        public UpAnswerNo(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "uprestartasdasdasdno", "upworld.restart");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            if (args.length == 1) {
                sender.sendMessage(ChatColor.GREEN + "Вы успешно отменили перезагрузку сервера \"" + ChatColor.YELLOW + args[0] + "\"!");
            } else {
                sender.sendMessage(ChatColor.GREEN + "Вы успешно отменили перезагрузку всех серверов!");
            }
        }
    }
}
