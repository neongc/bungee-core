package ru.upworld.commands;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.modules.UpWorldCommand;
import ru.upworld.api.enums.CommandAccess;

public class OnlineCommand extends UpWorldCommand {
    public OnlineCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "online");
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.GREEN + "Общий онлайн на проекте: " + BungeeCord.getInstance().getPlayers().size());
        if (sender.hasPermission("upworld.staff.online")){
            sender.sendMessage(ChatColor.GREEN + "");
        }
    }
}
