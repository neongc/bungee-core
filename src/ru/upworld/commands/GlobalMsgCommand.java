package ru.upworld.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.api.modules.UpWorldCommand;

public class GlobalMsgCommand extends UpWorldCommand {
    public GlobalMsgCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "globalmsg", "upworld.globalmsg");
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(ChatColor.RED + "Использование: /globalmsg <message>");
        }
        if (args.length < 2) {
            sender.sendMessage("§cУкажите тип и сообщение");
            return;
        }
        String message = ( "[YOUVIPAS WORLD] &6 "+ joiner(args)).replace("&", "§");
        UpWorldBungee.getInstance().getLoggerManager().log("User " + sender.getName() + " sent global message: " + message);
        ProxyServer.getInstance().broadcast(message);
    }

    private String joiner(String[] args) {
        String message = "";
        for (int i = 0;i < args.length;++i) {
            message = message + args[i] + " ";
        }
        return message;
    }
}
