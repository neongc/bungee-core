package ru.upworld.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.api.modules.UpWorldCommand;
import ru.upworld.managers.WhiteListManager;

public class UpWhitelistCommand extends UpWorldCommand {
    
    private static final String USAGE_REMOVE = ChatColor.AQUA + "Использование: /upwhitelist remove/delete <player>";
    private static final String USAGE_ADD = ChatColor.AQUA + "Использование: /upwhitelist add <player>";
    private static final String LIST_ALREADY_OFF = ChatColor.AQUA + "Белый спиок уже выключен!";
    private static final String LIST_ALREADY_ON = ChatColor.AQUA + "Белый спиок уже включен!";
    private static final String LIST_ON = ChatColor.AQUA + "Белый список включен!";
    private static final String LIST_OFF = ChatColor.AQUA + "Белый спиок выключен!";
    private static final String PLAYER_ADDED = ChatColor.AQUA + "Игрок добавлен в белый список!";
    private static final String PLAYER_REMOVED = ChatColor.AQUA + "Игрок удалён из белого списка!";
    private static final String PLAYER_EXISTS = ChatColor.AQUA + "Игрок уже есть в белом списке!";
    private static final String PLAYER_NOT_EXISTS = ChatColor.AQUA + "Игрока нет в белом списке!";
    private static final String LOG_LIST_OFF = ChatColor.DARK_RED + "Белый список выключен игроком: ";
    private static final String LOG_LIST_ON = ChatColor.DARK_RED + "Белый список включен игроком: ";

    public UpWhitelistCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "upwhitelist", "upworld.upwhitelist");
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        switch (args[0].toLowerCase()) {
            case "list":
                sender.sendMessage(ChatColor.AQUA + "UpWhiteList: " + WhiteListManager.getList());
                break;
            case "status":
                if (WhiteListManager.isEnabled()) {
                    sender.sendMessage(LIST_ON);
                } else {
                    sender.sendMessage(LIST_OFF);
                }
                break;
            case "on":
                if (!WhiteListManager.isEnabled()) {
                    WhiteListManager.setEnabled(true);
                    sender.sendMessage(LIST_ON);
                    UpWorldBungee.getInstance().getLoggerManager().log("UpWhiteList", LOG_LIST_ON + sender);
                } else {
                    sender.sendMessage(LIST_ALREADY_ON);
                }
                break;
            case "off":
                if (WhiteListManager.isEnabled()) {
                    WhiteListManager.setEnabled(false);
                    sender.sendMessage(LIST_OFF);
                    UpWorldBungee.getInstance().getLoggerManager().log("UpWhiteList", LOG_LIST_OFF + sender);
                } else {
                    sender.sendMessage(LIST_ALREADY_OFF);
                }
                break;
            case "add":
                if (args.length < 2) {
                    sender.sendMessage(USAGE_ADD);
                    break;
                }
                if (WhiteListManager.contains(args[1])) {
                    sender.sendMessage(PLAYER_EXISTS);
                } else {
                    WhiteListManager.addPlayer(args[1]);
                    sender.sendMessage(PLAYER_ADDED);
                }
                break;
            case "remove":
            case "delete":
                if (args.length < 2) {
                    sender.sendMessage(USAGE_REMOVE);
                    break;
                }
                if (!WhiteListManager.contains(args[1])) {
                    sender.sendMessage(PLAYER_NOT_EXISTS);
                } else {
                    WhiteListManager.removePlayer(args[1]);
                    sender.sendMessage(PLAYER_REMOVED);
                }
                break;
            case "help":
            default:
                sendHelp(sender);
                break;
        }
    }

    private void sendHelp(CommandSender sender) {
        sender.sendMessage(ChatColor.AQUA + "===" + ChatColor.GRAY + " UpWhiteList " + ChatColor.AQUA + " ===");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist list " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Посмотреть список игроков");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist status " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Посмотреть состояние белого списка");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist on " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Включить белый список");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist off " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Выключить белый список");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist add <player> " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Добавить игрока в белый список");
        sender.sendMessage(ChatColor.AQUA + "/upwhitelist <remove/delete> <player> " + ChatColor.GOLD + " - " + ChatColor.GREEN + "Удалить игрока из белого списка");

    }
}
