package ru.upworld.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.api.modules.UpWorldCommand;
import ru.upworld.utils.ChatUtil;

public class UpStopCommand extends UpWorldCommand {
    
    private static boolean command;

    public UpStopCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "upstop", "upworld.stop", true);
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.DARK_AQUA + "Вы уверены? Все сервера будут выключены!");
        sender.sendMessage(ChatUtil.printRunCommand(ChatColor.RED + "Да", "/upstopasdasdasdyes " + joiner(args)), new TextComponent(" "), ChatUtil.printRunCommand(ChatColor.GREEN + "Нет", "/upstopasdasdasdno"));
        command = true;
    }

    private String joiner(String[] args) {
        String message = "";
        for (int i = 0;i < args.length;++i) {
            message = message + args[i] + " ";
        }
        return message;
    }

    public static class UpStopYes extends UpWorldCommand {

        public UpStopYes(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "upstopasdasdasdyes", "upworld.stop");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            Integer i;
            try {
                i = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.GREEN + "Вы отменили выключение всех серверов!(аргумент не является числом)");
                return;
            }
            sender.sendMessage(ChatColor.DARK_RED + "Вы запустили выключение всех серверов!");
            UpWorldBungee.getInstance().getRestartManager().fullStop(args.length == 0 ? 60 : i);
        }
    }

    public static class UpStopNo extends UpWorldCommand {

        public UpStopNo(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "upstopasdasdasdno", "upworld.stop");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            sender.sendMessage(ChatColor.GREEN + "Вы успешно отменили выключение всех серверов!");
        }
    }
}
