package ru.upworld.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.api.modules.UpWorldCommand;
import ru.upworld.utils.ChatUtil;

public class UpTechWorksCommand extends UpWorldCommand {
    
    private static boolean command;

    public UpTechWorksCommand(UpWorldBungee core) {
        super(core, CommandAccess.ALL, "uptechworks", "upworld.tech");
    }

    @Override
    public void onCheckedCommand(CommandSender sender, String[] args) {
        sender.sendMessage(ChatColor.DARK_AQUA + "Вы уверены? Сервер будет закрыт на тех. работы!");
        sender.sendMessage(ChatUtil.printRunCommand(ChatColor.RED + "Да", "/uptechasdasdasdyes " + joiner(args)), new TextComponent(" "), ChatUtil.printRunCommand(ChatColor.GREEN + "Нет", "/uptechasdasdasdno"));
        command = true;
    }

    private String joiner(String[] args) {
        String message = "";
        for (int i = 0;i < args.length;++i) {
            message = message + args[i] + " ";
        }
        return message;
    }

    public static class UpTechWorksYes extends UpWorldCommand {

        public UpTechWorksYes(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "uptechasdasdasdyes", "upworld.tech");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            Integer i;
            try {
                i = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage(ChatColor.GREEN + "Вы отменили закрытие на тех. работы всех серверов!(аргумент не является числом)");
                return;
            }
            sender.sendMessage(ChatColor.DARK_RED + "Вы запустили закрытие на тех. работы всех серверов!");
            UpWorldBungee.getInstance().getRestartManager().fullTW(args.length == 0 ? 60 : i);
        }
    }

    public static class UpTechWorksNo extends UpWorldCommand {

        public UpTechWorksNo(UpWorldBungee core) {
            super(core, CommandAccess.ALL, "uptechasdasdasdno", "upworld.tech");
        }

        @Override
        public void onCheckedCommand(CommandSender sender, String[] args) {
            if (!command) return;
            command = false;
            sender.sendMessage(ChatColor.GREEN + "Вы успешно отменили закрытие на тех. работы всех серверов!");
        }
    }
}
