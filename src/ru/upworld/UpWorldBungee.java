package ru.upworld;

import java.util.HashMap;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Plugin;
import ru.upworld.api.ConfigManager;
import ru.upworld.api.ConnectDB;
import ru.upworld.commands.*;
import ru.upworld.data.DBs;
import ru.upworld.data.DataHandler;
import ru.upworld.data.ServerDataHandler;
import ru.upworld.log.LoggerManager;
import ru.upworld.managers.MessagesHandler;
import ru.upworld.managers.RestartManager;
import ru.upworld.managers.UsesManager;
import ru.upworld.managers.WhiteListManager;
import ru.upworld.network.ContainerClient;
import ru.upworld.utils.MySQLWorker;
import ru.upworld.utils.SetDefConfig;
import ru.upworld.utils.ShortCut;

public class UpWorldBungee extends Plugin {

    private static UpWorldBungee instance;
    public static ConfigManager coreconfig;
    private DBs dataBases;
    private ShortCut shortCut;
    private DataHandler dataHandler;
    private UsesManager usesManager;
    private ConnectDB connectDB;
    private LoggerManager loggerManager;
    private MessagesHandler messagesHandler;
    private RestartManager restartManager;
    public static boolean db_srv;
    public ContainerClient client;
    public static ServerDataHandler server;
    public static HashMap<String, ConfigManager> config = new HashMap();

    @Override
    public void onEnable(){
        instance = this;
        loggerManager = new LoggerManager();
        dataBases = new DBs();
        dataHandler = new DataHandler();
        shortCut = new ShortCut();
        getProxy().getPluginManager().registerCommand(this, new UpWhitelistCommand(this));
        getProxy().getPluginManager().registerCommand(this, new GlobalMsgCommand(this));
        getProxy().getPluginManager().registerCommand(this, new OnlineCommand(this));
        //
        getProxy().getPluginManager().registerCommand(this, new UpStopCommand(this));
        getProxy().getPluginManager().registerCommand(this, new UpStopCommand.UpStopYes(this));
        getProxy().getPluginManager().registerCommand(this, new UpStopCommand.UpStopNo(this));
        //
        getProxy().getPluginManager().registerCommand(this, new UpRestartCommand(this));
        getProxy().getPluginManager().registerCommand(this, new UpRestartCommand.UpAnswerYes(this));
        getProxy().getPluginManager().registerCommand(this, new UpRestartCommand.UpAnswerNo(this));
        //
        getProxy().getPluginManager().registerCommand(this, new UpTechWorksCommand(this));
        getProxy().getPluginManager().registerCommand(this, new UpTechWorksCommand.UpTechWorksYes(this));
        getProxy().getPluginManager().registerCommand(this, new UpTechWorksCommand.UpTechWorksNo(this));
        SetDefConfig.set();
        getLoggerManager().log("Configuration set");
        MySQLWorker.load();
        getLoggerManager().log("DB connected");
        WhiteListManager.load(this);
        usesManager = new UsesManager(this);
        getLoggerManager().log("Register events");
        if (!(getShortCut().getBool("mysql.this_srv.enabled"))) {
            db_srv = false;
        }
        client = new ContainerClient(getShortCut().getStr("proxy_server.ip"), getShortCut().getInt("proxy_server.port"));
        client.start();
    }

    @Override
    public void onDisable() {
        BungeeCord.getInstance().getPlayers().forEach((player) -> {
            player.disconnect(""
                    + "§6* * * * * * * * * * * * * * * * * * *"
                    + "\n§cСоединение разорвано"
                    + "\n"
                    + "\n§cПричина: §eСервер выключен"
                    + "\n§6* * * * * * * * * * * * * * * * * * *"
            );
        });
        client.stop();
        coreconfig.save();
        getDataBases().getDb_mg().stop();
        getDataBases().getDb_site().stop();
        if (db_srv) {
            getDataBases().getDb_this_srv().stop();
        }
        getLoggerManager().log("DB disconnected");
    }

    public DBs getDataBases() {
        return dataBases;
    }
    
    public DataHandler getDataHandler() {
        return dataHandler;
    }

    public UsesManager getUsesManager() {
        return usesManager;
    }
    
    public ShortCut getShortCut() {
        return shortCut;
    }

    public RestartManager getRestartManager() {
        return restartManager;
    }

    public MessagesHandler getMessagesHandler() {
        return messagesHandler;
    }

    public LoggerManager getLoggerManager() {
        return loggerManager;
    }

    public ConnectDB getConnectDB() {
        return connectDB;
    }

    public ConfigManager getConfigManager() {
        return coreconfig;
    }

    public static UpWorldBungee getInstance() {
        return instance;
    }
}
