package ru.upworld.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import ru.upworld.UpWorldBungee;

public class PlayerIdsSynchonyzed {

    private static final DBs dbs = UpWorldBungee.getInstance().getDataBases();

    /**
     * Получает класс игрока по айдишнику мини-игр базы
     *
     * @param player_id_mg
     * @return
     */
    public static Player getPlayerForIdmg(int player_id_mg) {
        String sql = "SELECT * FROM Players WHERE `id` = '" + player_id_mg + "'";
        String sqlSite = "select * from gamers where `id` =";
        ResultSet res;

        try {
            res = dbs.getDb_mg().getResultSql(sql);
            if (res.next()) {
                ResultSet res2 = dbs.getDb_site().getResultSql(sqlSite + dbs.getDb_mg().getInt("player_id", res));
                res2.next();
                return new Player(
                        dbs.getDb_mg().getInt("player_id", res),
                        dbs.getDb_site().getString("name", res2),
                        dbs.getDb_mg().getInt("server_id", res),
                        dbs.getDb_mg().getInt("server_last_id", res)
                );
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Получает игрока по глобальному ID
     *
     * @param player_id
     * @return
     */
    public static Player getPlayerForIdsite(int player_id) {
        String sql = "SELECT * FROM Players WHERE `player_id` = '" + player_id + "'";
        String sqlSite = "select * from gamers where `id` =";
        ResultSet res;

        try {
            res = dbs.getDb_mg().getResultSql(sql);
            if (res.next()) {
                ResultSet res2 = dbs.getDb_site().getResultSql(sqlSite + player_id);
                res2.next();
                return new Player(
                        dbs.getDb_mg().getInt("player_id", res),
                        dbs.getDb_site().getString("name", res2),
                        dbs.getDb_mg().getInt("server_id", res),
                        dbs.getDb_mg().getInt("server_last_id", res)
                );
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Получает глобальный ID игрока по имени
     *
     * @param name
     * @return
     */
    public static int getPlayerIdForName(String name) {
        String sql = "SELECT `id` FROM gamers WHERE `name` = '" + name + "'";
        ResultSet res;

        try {
            res = dbs.getDb_site().getResultSql(sql);
            if (res.next()) {
                return dbs.getDb_site().getInt("id", res);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    /**
     * Получает ID мини-игр игрока по имени
     *
     * @param name
     * @return
     */
    public static int getPlayerIdmgForName(String name) {
        int idGlobal = getPlayerIdForName(name);
        String sql = "SELECT `id` FROM Players WHERE `player_id` = '" + idGlobal + "'";
        ResultSet res;

        try {
            res = dbs.getDb_site().getResultSql(sql);
            if (res.next()) {
                return dbs.getDb_site().getInt("id", res);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    /**
     * Получение ID игрока глобального по ID мини-игры
     *
     * @param id
     * @return
     */
    public static int getPlayerIdForIdmg(int id) {
        String sql = "SELECT `player_id` FROM Players WHERE `id` = '" + id + "'";
        ResultSet res;

        try {
            res = dbs.getDb_site().getResultSql(sql);
            if (res.next()) {
                return dbs.getDb_site().getInt("player_id", res);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

}
