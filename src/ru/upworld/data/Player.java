package ru.upworld.data;

import net.md_5.bungee.BungeeCord;
import ru.upworld.api.enums.LoadType;
import ru.upworld.events.PlayerDisconnectEvent;
import ru.upworld.events.PlayerLoadEvent;

public class Player {

    private final String nickname;
    private final int playerId;
    private int server;
    private int server_last;
    
    private boolean loaded;

    public void load() {
        if (isLoaded()) {
            BungeeCord.getInstance().getPluginManager().callEvent(new PlayerLoadEvent(this, LoadType.JOIN_LOAD));
        } else {
            this.loaded = true;
            BungeeCord.getInstance().getPluginManager().callEvent(new PlayerLoadEvent(this, LoadType.FIRST_LOAD));
        }
    }

    public void unLoad() {
        BungeeCord.getInstance().getPluginManager().callEvent(new PlayerDisconnectEvent(this));
    }

    public Player(int plId, String nickname, int server, int server_last) {
        this.playerId = plId;
        this.nickname = nickname;
        this.server = server;
        this.server_last = server_last;
    }

    public String getNickname() {
        return nickname;
    }

    public int getPlayerId() {
        return playerId;
    }

    public int getServer() {
        return server;
    }

    public int getServer_last() {
        return server_last;
    }

    public void setServer(int server) {
        this.server = server;
    }

    public void setServer_last(int server_last) {
        this.server_last = server_last;
    }

    public boolean isLoaded() {
        return this.loaded;
    }
}
