package ru.upworld.data;

import ru.upworld.api.ConnectDB;

public class DBs {

    private ConnectDB db_site;
    private ConnectDB db_mg;
    private ConnectDB db_this_srv;

    public void setDb_site(String host, String user, String pass, Integer port, String db){
        db_site = new ConnectDB(host, user, pass, port, db);
    }

    public void setDb_mg(String host, String user, String pass, Integer port, String db){
        db_mg = new ConnectDB(host, user, pass, port, db);
    }

    public void setDb_this_srv(String host, String user, String pass, Integer port, String db){
        db_this_srv = new ConnectDB(host, user, pass, port, db);
    }

    public ConnectDB getDb_site() {
        return db_site;
    }

    public ConnectDB getDb_mg() {
        return db_mg;
    }

    public ConnectDB getDb_this_srv() {
        return db_this_srv;
    }
}
