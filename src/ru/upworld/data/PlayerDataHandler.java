package ru.upworld.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import net.md_5.bungee.BungeeCord;
import ru.upworld.UpWorldBungee;

public class PlayerDataHandler {

    private final DataHandler data;
    private final String player;
    private static final HashMap<String, Player> players = new HashMap();
    private final UpWorldBungee core = UpWorldBungee.getInstance();
    private final DBs dbs = core.getDataBases();

    public PlayerDataHandler(DataHandler data, String player) {
        this.data = data;
        this.player = player;
    }

    private void noPlayer() {
        BungeeCord.getInstance().getPlayers().forEach((p) -> {
            if (p.getName().equals(player)) {
                get(p.getName());
            }
        });
    }

    public Player getPlayer() {
        if (players.containsKey(player)) {
            return players.get(player);
        } else {
            noPlayer();
            return getPlayer();
        }
    }

    public void unLoad() {
        getPlayer().unLoad();
        players.remove(player);
    }

    public void setServer(int server) {
        this.setServer(player, server);
    }

    public void setServer_last(int server) {
        this.setServer_last(player, server);
    }

    public void setServer(String p, int server) {
        BungeeCord.getInstance().getScheduler().runAsync(core, () -> {
            try {
                String sql = "UPDATE Players SET `server_id` = '" + server + "' WHERE `player_id` = " + players.get(p).getPlayerId();
                dbs.getDb_mg().setUpdate(sql);
                players.get(p).setServer(server);
            } catch (SQLException ignored) {
            }
        });
    }

    public void setServer_last(String p, int server_last) {
        BungeeCord.getInstance().getScheduler().runAsync(core, () -> {
            try {
                String sql = "UPDATE Players SET `server_last_id` = '" + server_last + "' WHERE `player_id` = " + players.get(p).getPlayerId();
                dbs.getDb_mg().setUpdate(sql);
                players.get(p).setServer_last(server_last);
            } catch (SQLException ignored) {
            }
        });
    }

    public void load() {
        get(player);
    }

    private void get(String p) {
        try {
            //достаем оригинальный player_id и пользуемся им везде, передавая в класс игрока
            String sql = "select * from gamers where `name` = '" + p + "'";
            ResultSet res = dbs.getDb_site().getResultSql(sql);
            if (!res.next()) {
                System.out.println("res next = null");
                return;
            }
            int playerId = dbs.getDb_site().getInt("id", res);

            sql = "SELECT * FROM Players WHERE `player_id` = '" + playerId + "'";
            res = dbs.getDb_mg().getResultSql(sql);
            if (dbs.getDb_mg().getResultSetRowCount(res) == 0) {
                String upd = "INSERT INTO Players (`player_id`,`gamemode`, `fly`, `vanish`, `server_id`, `server_last_id`) VALUES (" + playerId + ",'ADVENTURE', 0, 0, " + UpWorldBungee.server.getServerId() + ", " + UpWorldBungee.server.getServerId() + ")";
                dbs.getDb_mg().setUpdate(upd);
                players.put(p, new Player(playerId, p, UpWorldBungee.server.getServerId(), UpWorldBungee.server.getServerId()));
            } else {
                int srv_id = 0;
                int srv_last = 0;
                if (res.next()) {
                    srv_id = dbs.getDb_mg().getInt("server_id", res);
                    srv_last = dbs.getDb_mg().getInt("server_last_id", res);
                }
                players.put(p, new Player(playerId, p, srv_id, srv_last));
            }
            getPlayer().load();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
