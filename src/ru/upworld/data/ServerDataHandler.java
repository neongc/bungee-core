package ru.upworld.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.ServerType;

public class ServerDataHandler {

    private String name;
    private int serverId;
    private ServerType type;

    public ServerDataHandler(int id, String name) {
        this.serverId = id;
        this.name = name;
    }

    public ServerDataHandler() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = ServerType.valueOf(type);
    }

    public void setType(ServerType type) {
        this.type = type;
    }

    public int getOtherServer(String name) {
        try {
            String sql = "Select id from Servers where b_name = '" + name + "'";
            ResultSet r = UpWorldBungee.getInstance().getDataBases().getDb_mg().getResultSql(sql);
            if (UpWorldBungee.getInstance().getDataBases().getDb_mg().getResultSetRowCount(r) > 0) {
                r.next();
                return UpWorldBungee.getInstance().getDataBases().getDb_mg().getInt("id", r);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public String getOtherServer(int id) {
        try {
            String sql = "Select b_name from Servers where id = " + id;
            ResultSet r = UpWorldBungee.getInstance().getDataBases().getDb_mg().getResultSql(sql);
            if (UpWorldBungee.getInstance().getDataBases().getDb_mg().getResultSetRowCount(r) > 0) {
                r.next();
                return UpWorldBungee.getInstance().getDataBases().getDb_mg().getString("b_name", r);
            }
        } catch (SQLException e) {
        }
        return "";
    }

    public String getName() {
        return name;
    }

    public int getServerId() {
        return serverId;
    }

    public ServerType getType() {
        return type;
    }

}
