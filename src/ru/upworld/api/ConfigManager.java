package ru.upworld.api;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class ConfigManager {

    private Configuration config;
    private String fileName;
    public File folder;
    private final String name = "[ConfigManager]";
    public HashMap<String, String> str = new HashMap<>();
    public HashMap<String, Integer> integers = new HashMap<>();
    public HashMap<String, Boolean> bol = new HashMap<>();
    public HashMap<String, List<?>> arrays = new HashMap<>();

    public ConfigManager(File dataFolder) {
        folder = dataFolder;
    }

    public boolean load(String FileName) {
        fileName = FileName;
        File file = new File(folder, FileName);
        try {
            if (!folder.exists()) {
                sendLog("Создаю папку " + folder.getName());
                folder.mkdir();
            }
            if (!file.exists()) {
                sendLog("Создаю файл " + FileName);
                file.createNewFile();
            }
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            sendLog("Успешная загрузка файла " + FileName);
            return true;
        } catch (IOException ex) {
            sendLog("Невозможно загрузить файл " + FileName);
            return false;
        }
    }

    public boolean save() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, new File(folder, fileName));
            sendLog("Успешное сохранение файла " + fileName);
            return true;
        } catch (IOException ex) {
            sendLog("Невозможно сохранить файл " + fileName);
            return false;
        }
    }

    public boolean loadDefault(InputStream file) {
        if (!folder.exists()) {
            folder.mkdir();
            sendLog("Папка " + folder.getAbsolutePath() + " не существует и была создана.");
        }
        if (!new File(folder, fileName).exists()) {
            try {
                Files.copy(file, new File(folder, fileName).toPath(), StandardCopyOption.REPLACE_EXISTING);
                sendLog("Файл " + fileName + " был взят из потока.");
                return true;
            } catch (IOException ex) {
                sendLog("Файл " + fileName + " нельзя записать в папку.");
                return false;
            }
        }
        return false;
    }

    public void reload() {
        load(fileName);
        str.clear();
        integers.clear();
        bol.clear();
        arrays.clear();
        sendLog("Файл " + fileName + " перезагружен");
    }

    public String getStr(String path) {
        if (str.containsKey(path)) {
            return str.get(path);
        } else {
            String ret = config.getString(path);
            str.put(path, ret);
            return ret;
        }
    }

    public Integer getInt(String path) {
        if (integers.containsKey(path)) {
            return integers.get(path);
        } else {
            Integer ret = config.getInt(path);
            integers.put(path, ret);
            return ret;
        }
    }

    public Boolean getBool(String path) {
        if (bol.containsKey(path)) {
            return bol.get(path);
        } else {
            Boolean ret = config.getBoolean(path);
            bol.put(path, ret);
            return ret;
        }
    }

    public List getArr(String path) {
        if (arrays.containsKey(path)) {
            return arrays.get(path);
        } else {
            List ret = config.getList(path);
            arrays.put(path, ret);
            return ret;
        }
    }

    public void setStr(String path, String str) {
        if (this.str.containsKey(path)) {
            this.str.remove(path);
        }
        this.str.put(path, str);
        config.set(path, str);
    }

    public void setBool(String path, boolean bool) {
        if (this.bol.containsKey(path)) {
            this.bol.remove(path);
        }
        this.bol.put(path, bool);
        config.set(path, bool);
    }

    public void setInt(String path, Integer in) {
        if (this.integers.containsKey(path)) {
            this.integers.remove(path);
        }
        this.integers.put(path, in);
        config.set(path, in);
    }

    public void setArr(String path, List<?> s) {
        if (this.arrays.containsKey(path)) {
            this.arrays.remove(path);
        }
        this.arrays.put(path, s);
        config.set(path, s);
    }

    public boolean checkPath(String path) {
        return config.contains(path);
    }

    public boolean searchInArray(String path, Integer num) {
        List<?> buf = getArr(path);
        if (buf == null) {
            return false;
        }
        if (buf.isEmpty()) {
            return false;
        }
        int Position = buf.indexOf(num);
        return Position >= 0;
    }

    public boolean searchInArray(String path, String num) {
        List<?> buf = getArr(path);
        if (buf == null) {
            return false;
        }
        if (buf.isEmpty()) {
            return false;
        }
        int Position = buf.indexOf(num);
        return Position >= 0;
    }

    private void sendLog(String log) {
        Logger.getAnonymousLogger().log(Level.OFF, name + " {0}", log);
    }

}

 