package ru.upworld.api.modules;

import java.beans.ConstructorProperties;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.CommandAccess;
import ru.upworld.network.ContainerClient;
import ru.upworld.utils.ChatUtil;

public abstract class UpWorldCommand extends Command {
    
    private final UpWorldBungee core;
    private final CommandAccess access;
    private final String permission;
    private final boolean packets;
    
    private final String onAccess = ChatColor.RED + "Доступ к этой команде из консоли запрещён!";
    private final String onAccessConsole = ChatColor.RED + "Доступ к этой команде разрешён только из консоли!";
    private final String onAccessPerm = ChatColor.RED + "Недостаточно прав для выполнения данной команды!";
    private final String onCoreConnect = ChatColor.RED + "Нет соединения с сервером! Пожалуйста сообщите об этом администратору: minigames@up-world.ru";

    @ConstructorProperties({"core", "access", "command", "permission", "packets"})
    public UpWorldCommand(UpWorldBungee core, CommandAccess access, String name, String permission, boolean packets) {
        super(name, null, new String[0]);
        this.core = core;
        this.access = access;
        this.permission = permission;
        this.packets = packets;
    }

    public UpWorldBungee getCore() {
        return this.core;
    }
    
    public UpWorldCommand(UpWorldBungee core, CommandAccess access, String name, String permission) {
        this(core, access, name, permission, false);
    }
    
    public UpWorldCommand(UpWorldBungee core, CommandAccess access, String name, boolean packets) {
        this(core, access, name, null, packets);
    }

    public UpWorldCommand(UpWorldBungee core, CommandAccess access, String name) {
        this(core, access, name, null);
    }

    @Override
    public void execute(CommandSender sender,  String[] args) {
        if ((!(sender instanceof ProxiedPlayer)) && (this.access == CommandAccess.PLAYER)) {
            sender.sendMessage(onAccess);
            return;
        } else if (this.access == CommandAccess.CONSOLE) {
            sender.sendMessage(onAccessConsole);
            return;
        } else {
            if ((this.permission != null) && (!sender.hasPermission(this.permission))) {
                sender.sendMessage(onAccessPerm);
                return;
            }
        }
        if (packets && !core.client.isRunning(ContainerClient.Action.CONNECTED)) {
            sender.sendMessage(ChatUtil.printShow_OpenUrl(onCoreConnect, "Или сюда: https://vk.com/mneon512 (жмякай)", "https://vk.com/mneon512"));
            return;
        }
        onCheckedCommand(sender, args);
    }

    public abstract void onCheckedCommand(CommandSender paramCommandSender, String[] paramArrayOfString);
}
