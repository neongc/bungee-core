package ru.upworld.api.enums;

public enum CommandAccess {
    PLAYER,  CONSOLE, ALL;

    private CommandAccess() {}
}
