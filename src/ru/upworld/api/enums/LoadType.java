package ru.upworld.api.enums;

public enum LoadType {

    FIRST_LOAD, JOIN_LOAD;

    private LoadType() {
    }
}
