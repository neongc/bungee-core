package ru.upworld.api.enums;

public enum ServerType {
    MINIGAME, LOBBY, HUB, STAFF, LIMBO, BUNGEE;

    private ServerType() {}
}