package ru.upworld.api;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class sqlConfig {

    public Connection conn;
    public Statement statmt;
    public ResultSet resSet;

    public sqlConfig() {
        try {
            conn = null;
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite::memory:");
            statmt = conn.createStatement();
            createTables();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(sqlConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public sqlConfig(String nameFile) {
        try {
            conn = null;
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:" + nameFile + ".db");
            statmt = conn.createStatement();
            createTables();
        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(sqlConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createTables() throws SQLException {
        String sql = "";
        sql = "CREATE TABLE IF NOT EXISTS `config`(`id` INTEGER PRIMARY KEY AUTOINCREMENT,`name`TEXT NOT NULL,`type`INTEGER NOT NULL,`value`TEXT NOT NULL);";
        statmt.execute(sql);
    }

    public void addUpdateConfig(String name, int type, Object value) throws SQLException {
        resSet = statmt.executeQuery("select count(*) as `count` from `config` where `name` ='" + name + "'");
        resSet.next();
        if (resSet.getInt("count") > 0) {
            statmt.execute("UPDATE `config` SET `value`=" + value + " WHERE `id`=" + resSet.getInt("id") + ";");
        } else {
            statmt.execute("INSERT INTO `config`(`id`,`name`,`type`,`value`) VALUES (NULL,'" + name + "'," + type + ",'" + value + "');");
        }
    }

    public Object getConfigValue(String name) throws SQLException {
        resSet = statmt.executeQuery("select count(*) as `count`,* from `config` where `name` ='" + name + "'");
        resSet.next();
        if (resSet.getInt("count") > 0) {
            return (Object) resSet.getString("vaule");
        }
        return null;
    }

}
