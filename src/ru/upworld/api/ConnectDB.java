package ru.upworld.api;

import static java.lang.Thread.sleep;
import java.sql.*;
import java.util.logging.Level;
import net.md_5.bungee.BungeeCord;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.ConnectDB.callbackInsertUpdate;

public class ConnectDB {

    private final String host;
    private final String user;
    private final String port;
    private final String db;
    private final String pass;
    private java.sql.Statement statement;
    private Connection conn = null;
    private long lastSend = 0;
    private final checkClouseConnect sheduler = new checkClouseConnect();

    public ConnectDB(String host, String user, String pass, Integer port, String db) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.port = port.toString();
        this.db = db;
        sheduler.start();
    }

    private void updateTimeStamp() {
        lastSend = System.currentTimeMillis() / 1000L;
    }

    private void checkTimeStamp() {
        long check = System.currentTimeMillis() / 1000L;
        if ((check - lastSend) >= 60) {
            stop();
            sheduler.connect = false;
        }
    }

    private class checkClouseConnect extends Thread {

        public int delay = 30;
        public boolean connect = false;

        @Override
        public void run() {
            while (true) {
                if (connect) {
                    checkTimeStamp();
                }
                try {
                    sleep((long) delay * 1000);
                } catch (InterruptedException ex) {
                }
            }
        }
    }

    public void connect() throws ClassNotFoundException, SQLException {
        String url = "jdbc:mysql://" + host + ":" + port + "/" + db + "?autoReconnect=true&failOverReadOnly=false&maxReconnects=10";
        try {
            conn = DriverManager.getConnection(url, user, pass);
            statement = conn.createStatement();
            if (!sheduler.connect) {
                updateTimeStamp();
                sheduler.connect = true;
            }
        } catch (SQLException sqlEx) {
            sqlEx.printStackTrace();
        }
    }

    public void stop() {
        if (conn != null) {
            try {
                conn.close();
                UpWorldBungee.getInstance().getLogger().log(Level.WARNING, "Database {0} connection terminated", db);
            } catch (SQLException ignored) {
            }
        }
    }

    public ResultSet getResultSql(String sql) throws SQLException {
        tryReconnect();
        return statement.executeQuery(sql);
    }

    public String getString(String colomn, ResultSet resul) throws SQLException {
        return resul.getString(findColomn(colomn, resul));
    }

    public int getInt(String colomn, ResultSet resul) throws SQLException {
        return resul.getInt(findColomn(colomn, resul));
    }

    public boolean getBool(String colomn, ResultSet resul) throws SQLException {
        String res = resul.getString(findColomn(colomn, resul));
        return (!res.equals("0")) && (!res.equals("false"));
    }

    private Integer findColomn(String colomn, ResultSet resul) throws SQLException {
        return resul.findColumn(colomn);
    }

    public int getResultSetRowCount(ResultSet result) throws SQLException {
        if (result != null) {
            result.last();
            int rsSize = result.getRow();
            result.beforeFirst();
            return rsSize;
        }
        return 0;
    }

    public void setUpdate(String sql) throws SQLException {
            try {
                tryReconnect();
                statement.executeUpdate(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
    }

    public interface callbackInsertUpdate {

        public void returned(int id);
    }

    public void setUpdateAndGetId(String sql, callbackInsertUpdate handler) {
        BungeeCord.getInstance().getScheduler().runAsync(UpWorldBungee.getInstance(), () -> {
            try {
                tryReconnect();
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = statement.getGeneratedKeys();
                if (rs.next()) {
                    handler.returned(rs.getInt(1));
                } else {
                    handler.returned(0);
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        });
    }

    private void tryReconnect() throws SQLException {
        updateTimeStamp();
        if (conn.isClosed()) {
            try {
                connect();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (statement.isClosed()) {
            statement = conn.createStatement();
        }
    }
}
