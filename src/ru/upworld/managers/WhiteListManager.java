package ru.upworld.managers;

import java.util.ArrayList;
import java.util.List;
import ru.upworld.UpWorldBungee;

public class WhiteListManager {
    
    private static boolean enabled;
    private static List<String> players = new ArrayList<>();
    private static String message;

    public static String getMessage() {
        return message;
    }

    public static void setMessage(String msg) {
        message = msg;
    }
    
    public static boolean contains(String player) {
        return players.contains(player);
    }
    
    public static void addPlayer(String player) {
        players.add(player);
    }
    
    public static void removePlayer(String player) {
        players.remove(player);
    }

    public static List<String> getList() {
        return players;
    }

    public static void setList(List<String> list) {
        players = list;
    }

    public static boolean isEnabled() {
        return enabled;
    }

    public static void setEnabled(boolean value) {
        enabled = value;
    }
    
    public static void load(UpWorldBungee core) {
        enabled = core.getConfigManager().getBool("whitelist.enabled");
        message = core.getConfigManager().getStr("whitelist.message");
        players = core.getConfigManager().getArr("whitelist.players");
    }
    
    public static void unLoad(UpWorldBungee core) {
        core.getConfigManager().setBool("whitelist.enabled", enabled);
        core.getConfigManager().setStr("whitelist.message", message);
        core.getConfigManager().setArr("whitelist.players", players);
    }

}
