package ru.upworld.managers;

import java.io.*;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class MessagesHandler implements Listener{

    @EventHandler
    public void PluginMessageEvent(PluginMessageEvent event) {
        if (event.getTag().equalsIgnoreCase("BungeeCord")) {
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(event.getData()));
            try {
                String subchannel = in.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendRestart(String message) {
        ProxyServer.getInstance().getServers().values().forEach((serverInfo) -> {
            sendDataToSpigot("Restart", serverInfo, message);
        });
    }

    public void sendDataToSpigot(String subchannel, ServerInfo spigot, String ... message) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(bytes);
        try {
            out.writeUTF(subchannel);
            for (String msg : message) {
                out.writeUTF(msg);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        spigot.sendData("Spigot", bytes.toByteArray());
    }
}
