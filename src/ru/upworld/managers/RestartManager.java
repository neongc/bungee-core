package ru.upworld.managers;

import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import ru.upworld.UpWorldBungee;

public class RestartManager {
    ScheduledTask reloadTask = null;
    ScheduledTask stopTask = null;

    public void fullRestart(final int countdown) {
        this.reloadTask = ProxyServer.getInstance().getScheduler().schedule(UpWorldBungee.getInstance(), new Runnable(){
            int counter = countdown + 1;
            @Override
            public void run() {
                this.counter -= 1;
                if (this.counter == 600) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 10 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 300) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 5 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 120) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 2 минуты!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 60) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 1 минуту!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 30) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 30 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 15) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 15 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 10) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Перезапуск сервера через 10 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 5) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунд до перезапуска сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 4) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до перезапуска сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 3) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до перезапуска сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 2) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до перезапуска сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 1) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунда до перезапуска сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 0) {
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        player.disconnect(TextComponent.fromLegacyText("" 
                                + ChatColor.GOLD
                                + "Сервер перезагружается"
                                + "\n"
                                + ChatColor.GOLD
                                + "Ждём вас через 1 минуту =)"
                        ));
                    });
                }
                if (this.counter == -2) {
                    reloadTask.cancel();
                    ProxyServer.getInstance().stop(ChatColor.RED + "Сервер перезагружается.");
                }
            }
        }, 1L, 1L, TimeUnit.SECONDS);
    }

    public void fullTW(final int contdown) {
        this.stopTask = ProxyServer.getInstance().getScheduler().schedule(UpWorldBungee.getInstance(), new Runnable() {
            int counter = contdown + 1;
            @Override
            public void run() {
                this.counter -= 1;
                if (this.counter == 600) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 10 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 300) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 5 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 120) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 2 минуты!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 60) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 1 минуту!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 30) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 30 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 15) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 15 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 10) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Закрытие сервера на тех. обслуживание через 10 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 5) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунд до закрытия сервера на тех. обслуживание!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 4) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до закрытия сервера на тех. обслуживание!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 3) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до закрытия сервера на тех. обслуживание!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 2) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до закрытия сервера на тех. обслуживание!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 1) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунда до закрытия сервера на тех. обслуживание!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 0) {
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        player.disconnect(TextComponent.fromLegacyText(ChatColor.GOLD + "Сервер закрыт на тех. обслуживание."));
                    });
                }
                if (this.counter == -2) {
                    stopTask.cancel();
                    WhiteListManager.setEnabled(true);
                }
            }
        }, 1L, 1L, TimeUnit.SECONDS);
    }

    public void fullStop(final int contdown) {
        this.stopTask = ProxyServer.getInstance().getScheduler().schedule(UpWorldBungee.getInstance(), new Runnable() {
            int counter = contdown + 1;
            @Override
            public void run() {
                this.counter -= 1;
                if (this.counter == 600) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 10 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 300) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 5 минут!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 120) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 2 минуты!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 60) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 1 минуту!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 30) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 30 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 15) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 15 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 10) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "Внимание!"));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "Остановка сервера через 10 секунд!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 5) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунд до остановки сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 4) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до остановки сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 3) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до остановки сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 2) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунды до остановки сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 1) {
                    Title title;
                    title = ProxyServer.getInstance().createTitle();
                    title.fadeIn(10);
                    title.fadeOut(20);
                    title.stay(100);
                    title.title(TextComponent.fromLegacyText(ChatColor.RED + "" + this.counter));
                    title.subTitle(TextComponent.fromLegacyText(ChatColor.GOLD + "секунда до остановки сервера!"));
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        title.send(player);
                    });
                }
                if (this.counter == 0) {
                    ProxyServer.getInstance().getPlayers().forEach((player) -> {
                        player.disconnect(TextComponent.fromLegacyText(ChatColor.GOLD + "Сервер выключен."));
                    });
                }
                if (this.counter == -2) {
                    stopTask.cancel();
                    ProxyServer.getInstance().stop(ChatColor.RED + "Сервер выключен.");
                }
            }
        }, 1L, 1L, TimeUnit.SECONDS);
    }
}
