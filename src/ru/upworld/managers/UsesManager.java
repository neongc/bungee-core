package ru.upworld.managers;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;
import ru.upworld.UpWorldBungee;
import ru.upworld.data.PlayerDataHandler;
import ru.upworld.network.handlers.ClientHandler;
import ru.upworld.network.packets.player.PlayerLoginPacket;

public class UsesManager implements Listener {
    
    private final UpWorldBungee core;
    
    public UsesManager(UpWorldBungee core) {
        this.core = core;
        BungeeCord.getInstance().getPluginManager().registerListener(core, this);
    }

    @EventHandler
    public void onPreLoginEvent(LoginEvent e) {
        if (!WhiteListManager.isEnabled()) return;
        String kickMessage = ChatColor.translateAlternateColorCodes('&', WhiteListManager.getMessage());
        if (!WhiteListManager.contains(e.getConnection().getName())) {
            e.setCancelled(true);
            e.setCancelReason(kickMessage);
        }
    }
    
    @EventHandler
    public void onPreLoginEvent(PreLoginEvent e) {
        ClientHandler.sendPacket(new PlayerLoginPacket(e.getConnection().getName(), e.getConnection().getAddress().toString()));
        PlayerDataHandler dataHandler = core.getDataHandler().getPlayerDataHandler(e.getConnection().getName());
        dataHandler.load();
    }
}
