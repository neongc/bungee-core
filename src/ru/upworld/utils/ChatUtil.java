package ru.upworld.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class ChatUtil {
    
    private static final Pattern chatColorPattern = Pattern.compile("(?i)&([0-9A-F])");
    private static final Pattern chatMagicPattern = Pattern.compile("(?i)&([K])");
    private static final Pattern chatBoldPattern = Pattern.compile("(?i)&([L])");
    private static final Pattern chatStrikethroughPattern = Pattern.compile("(?i)&([M])");
    private static final Pattern chatUnderlinePattern = Pattern.compile("(?i)&([N])");
    private static final Pattern chatItalicPattern = Pattern.compile("(?i)&([O])");
    private static final Pattern chatResetPattern = Pattern.compile("(?i)&([R])");

    public static TextComponent printRun_Show(String chatMessage, String runCommand, String showText) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, runCommand));
        chatBase.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(showText).create()));
        return chatBase;
    }

    public static TextComponent printSuggest_Show(String chatMessage, String suggestCommand, String showText) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, suggestCommand));
        chatBase.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(showText).create()));
        return chatBase;
    }
    
    public static TextComponent printShow_OpenUrl(String chatMessage, String showText, String openUrl) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(showText).create()));
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, openUrl));
        return chatBase;
    }

    public static TextComponent printOpenUrl(String chatMessage, String openUrl) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, openUrl));
        return chatBase;
    }

    public static TextComponent printOpenFile(String chatMessage, String openFile) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, openFile));
        return chatBase;
    }

    public static TextComponent printRunCommand(String chatMessage, String runCommand) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, runCommand));
        return chatBase;
    }

    public static TextComponent printSuggestCommand(String chatMessage, String suggestCommand) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, suggestCommand));
        return chatBase;
    }

    public static TextComponent printChangePage(String chatMessage, String changePage) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setClickEvent(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, changePage));
        return chatBase;
    }

    public static TextComponent printShowText(String chatMessage, String showText) {
        TextComponent chatBase = new TextComponent(chatMessage);
        chatBase.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(showText).create()));
        return chatBase;
    }
    
    public static String translateColorCodes(String string){
        if (string == null) {
            return "";
        }
        String newstring = string;
        newstring = chatColorPattern.matcher(newstring).replaceAll("§$1");
        newstring = chatMagicPattern.matcher(newstring).replaceAll("§$1");
        newstring = chatBoldPattern.matcher(newstring).replaceAll("§$1");
        newstring = chatStrikethroughPattern.matcher(newstring).replaceAll("§$1");
        newstring = chatUnderlinePattern.matcher(newstring).replaceAll("§$1");
        newstring = chatItalicPattern.matcher(newstring).replaceAll("§$1");
        newstring = chatResetPattern.matcher(newstring).replaceAll("§$1");
        return newstring;
    }
    
    public static List<String> border(List<String> str) {
        ArrayList<String> border = new ArrayList<>();
        border.add("§e===============================");
        border.add("");
        border.addAll(str);
        border.add("");
        border.add("§e===============================");
        return border;
    }

    public static List<String> border(String title, List<String> str) {
        ArrayList<String> border = new ArrayList<>();
        border.add("§a" + title.toUpperCase());
        border.add("");
        border.addAll(str);
        return border(border);
    }
    
    
}
