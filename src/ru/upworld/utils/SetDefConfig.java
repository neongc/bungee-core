package ru.upworld.utils;

import ru.upworld.UpWorldBungee;
import ru.upworld.api.ConfigManager;

public class SetDefConfig {
    
    public static void set() {
        ConfigManager cfg = new ConfigManager(UpWorldBungee.getInstance().getDataFolder());
        cfg.load("config.yml");
        //
        cfg.setStr("proxy_server.ip", "localhost");
        cfg.setInt("proxy_server.port", 10000);
        //
        cfg.setStr("mysql.site.host", "localhost");
        cfg.setInt("mysql.site.port", 3306);
        cfg.setStr("mysql.site.user", "admin_minesite");
        cfg.setStr("mysql.site.pass", "tlDAb2dgJw");
        cfg.setStr("mysql.site.db", "admin_minesite");
        //
        cfg.setStr("mysql.mg.host", "localhost");
        cfg.setInt("mysql.mg.port", 3306);
        cfg.setStr("mysql.mg.user", "mgn_global");
        cfg.setStr("mysql.mg.pass", "McKhLJlP3l");
        cfg.setStr("mysql.mg.db", "mgn_global");
        //
        cfg.setBool("mysql.this_srv.enabled", false);
        cfg.setStr("mysql.this_srv.host", "localhost");
        cfg.setInt("mysql.this_srv.port", 3306);
        cfg.setStr("mysql.this_srv.user", "mgn_skywars");
        cfg.setStr("mysql.this_srv.pass", "U4RwuO1U5i");
        cfg.setStr("mysql.this_srv.db", "mgn_skywars");
        //
        cfg.setStr("server.name", "MiniGames");
        cfg.setInt("server.id", 15);
        cfg.setBool("server.stock", true);
        cfg.setBool("whitelist.enabled", false);
        cfg.setStr("whitelist.message", ""
                        + "§6* * * * * * * * * * * * * * * * * * *"
                        + "\n§cСоединение разорвано"
                        + "\n"
                        + "\n§cПричина: §eНа сервере проводятся технические работы"
                        + "\n§6* * * * * * * * * * * * * * * * * * *");
        cfg.setArr("whitelist.players", null);
        cfg.save();
        UpWorldBungee.config.put("CORE", cfg);
        UpWorldBungee.coreconfig = UpWorldBungee.config.get("CORE");
    }
}
