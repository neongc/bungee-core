package ru.upworld.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ListenerInfo;
import ru.upworld.UpWorldBungee;
import ru.upworld.api.enums.ServerType;
import ru.upworld.data.DBs;
import ru.upworld.data.ServerDataHandler;

public class MySQLWorker {
    
    private static final DBs dbs = UpWorldBungee.getInstance().getDataBases();

    public static void load() {
        try {
            en();
            loadInfo();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    private static void en() throws ClassNotFoundException, SQLException {
        ShortCut scfg = UpWorldBungee.getInstance().getShortCut();

        dbs.setDb_site(scfg.getStr("mysql.site.host"), scfg.getStr("mysql.site.user"), scfg.getStr("mysql.site.pass"), scfg.getInt("mysql.site.port"), scfg.getStr("mysql.site.db"));
        dbs.getDb_site().connect();
        //
        dbs.setDb_mg(scfg.getStr("mysql.mg.host"), scfg.getStr("mysql.mg.user"), scfg.getStr("mysql.mg.pass"), scfg.getInt("mysql.mg.port"), scfg.getStr("mysql.mg.db"));
        dbs.getDb_mg().connect();
        //
        if (UpWorldBungee.db_srv) {
            dbs.setDb_this_srv(scfg.getStr("mysql.this_srv.host"), scfg.getStr("mysql.this_srv.user"), scfg.getStr("mysql.this_srv.pass"), scfg.getInt("mysql.this_srv.port"), scfg.getStr("mysql.this_srv.db"));
            dbs.getDb_this_srv().connect();
        }
    }

    private static void loadInfo() throws SQLException {
        ListenerInfo info = BungeeCord.getInstance().getConfig().getListeners().iterator().next();
        UpWorldBungee.getInstance().getLoggerManager().log(info.getHost().getAddress() + ":" + info.getHost().getPort());
        String sql = "SELECT * FROM Servers WHERE `ip` = '" + info.getHost().getAddress() + "' AND `port` = '" + info.getHost().getPort() + "'";
        ResultSet res = dbs.getDb_mg().getResultSql(sql);
        if (dbs.getDb_mg().getResultSetRowCount(res) != 0) {
            res.next();
            UpWorldBungee.server = new ServerDataHandler(dbs.getDb_mg().getInt("id", res), dbs.getDb_mg().getString("b_name", res));
            giveType(UpWorldBungee.server, dbs.getDb_mg().getString("type", res), dbs.getDb_mg().getString("b_name", res));
        } else {
            sql = "INSERT INTO Servers (`ip`, `port`, `type`, `b_name`, `spawn`) VALUES ('" + info.getHost().getAddress() + "', '" + info.getHost().getPort() + "', '" + ServerType.BUNGEE.name() + "', 'bungee1', '')";
            dbs.getDb_mg().setUpdate(sql);
            UpWorldBungee.server = new ServerDataHandler();
            giveType(UpWorldBungee.server, null, null);
        }
    }
    
    private static void giveType(ServerDataHandler serv, String type, String name) {
        ServerType typed;
        if (type != null && name != null && !type.equals("") && !name.equals("")) {
            typed = ServerType.valueOf(type.toUpperCase());
        } else {
            typed = ServerType.BUNGEE;
            name = "bungee1";
            UpWorldBungee.getInstance().getLoggerManager().log("Table server is empty");
        }
        serv.setType(typed);
        serv.setName(name);
    }
    
}