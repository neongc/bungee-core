package ru.upworld.utils;

import java.util.List;
import ru.upworld.UpWorldBungee;

public class ShortCut {

    public String getStr(String str) {
        return UpWorldBungee.coreconfig.getStr(str);
    }

    public Integer getInt(String str) {
        return UpWorldBungee.coreconfig.getInt(str);
    }

    public Boolean getBool(String str) {
        return UpWorldBungee.coreconfig.getBool(str);
    }

    public List<?> getArr(String str) {
        return UpWorldBungee.coreconfig.getArr(str);
    }
}
